﻿namespace ClinicSync.Application.DTO
{
    public record AppointmentCalednarRequest(
                                     DateTime firstDayOfWeek,
                                     DateTime lastDayOfWeek
                                    );
}
