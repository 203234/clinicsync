﻿using ClinicSync.Domain.Enums;

namespace ClinicSync.Application.DTO
{
    public record AppointmentRequest(
                                     int? AppointmentTemplateId,
                                     int? ClientId,
                                     AppointmentTemplateRequest? TemplateRequest,
                                     ClientRequest? ClientRequest,
                                     DateTime Date,
                                     int DurationInMinutes
                                    );
}
