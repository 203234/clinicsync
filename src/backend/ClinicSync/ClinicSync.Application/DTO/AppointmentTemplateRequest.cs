﻿namespace ClinicSync.Application.DTO
{
    public class AppointmentTemplateRequest()
    {
        public string Name { get; set; }
        public double? Price { get; set; }
        public string? Description { get; set; }
        public int? DurationInMinutes { get; set; }
    }
}
