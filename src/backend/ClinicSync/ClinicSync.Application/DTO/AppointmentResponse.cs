﻿using ClinicSync.Domain.Entities;
using System.Text.Json.Serialization;

namespace ClinicSync.Application.DTO
{
    public class AppointmentResponse
    {
        public Appointment Appointment { get; set; } = new Appointment();
        public int Col { get; set; }
        public int ColSpan { get; set; }
        public int Row { get; set; }
    }
}
