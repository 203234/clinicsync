﻿using ClinicSync.Domain.Enums;

namespace ClinicSync.Application.DTO
{
    public record ClientRequest(string FirstName,
                                string LastName,
                                string? Adress,
                                string? Phone,
                                string? Email,
                                string? DateOfBirth,
                                Gender? Gender){}
}
