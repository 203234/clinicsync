﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicSync.Application.DTO
{
    // More attributes to be added in the future
    public record CreateUserRequest(string Username, string Password);
}
