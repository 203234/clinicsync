﻿using ClinicSync.Application.Interfaces.services;
using ClinicSync.Application.Services;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace ClinicSync.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            var assembly = typeof(DependencyInjection).Assembly;

            services.AddMediatR(configuration => 
                configuration.RegisterServicesFromAssembly(assembly));

            services.AddValidatorsFromAssembly(assembly);

            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IAppointmentTemplateService, AppointmentTemplateService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IClientNotesService, ClientNoteService>();
            services.AddScoped<IAppointmentService, AppointmentService>();
            return services;
        }

    }
}