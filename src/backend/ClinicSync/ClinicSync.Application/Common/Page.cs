﻿using ClinicSync.Domain;

namespace ClinicSync.Application.Common
{
    public class Page<T>
    {
        public IEnumerable<T> Content { get; set; } = Enumerable.Empty<T>();
        public int PageNumber { get; set; } = 0;
        public int PageSize { get; set; } = AppConstants.PageSize;
        public int TotalElements { get; set; } = 0;
        public int TotalPages => (int)Math.Ceiling((double)TotalElements / PageSize);
    }
}
