﻿namespace ClinicSync.Application.Common
{
    public class Result<T>
    {
        public bool IsSuccess { get; }
        public bool IsFailure => !IsSuccess;
        public T Data { get; }
        public string ErrorMessage { get; }
        public IEnumerable<string> ErrorMessages { get; }

        protected Result(bool isSuccess, T data, string errorMessage, IEnumerable<string> errorMessages)
        {
            IsSuccess = isSuccess;
            Data = data;
            ErrorMessage = errorMessage;
            ErrorMessages = errorMessages;
        }

        public static Result<T> Success(T data) => new(true, data, null, null);

        public static Result<T> Failure(string errorMessage) => new(false, default, errorMessage, null);

        public static Result<T> Failure(IEnumerable<string> errorMessages) => new(false, default, null, errorMessages);
    }
}
