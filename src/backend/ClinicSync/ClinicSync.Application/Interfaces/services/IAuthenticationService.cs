﻿using ClinicSync.Application.Common;
using ClinicSync.Application.DTO;
using ClinicSync.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicSync.Application.Interfaces.services
{
    public interface IAuthenticationService
    {
        Task<Result<string>> Register(CreateUserRequest user);
        Task<Result<string>> Login(LoginRequest user);
    }
}
