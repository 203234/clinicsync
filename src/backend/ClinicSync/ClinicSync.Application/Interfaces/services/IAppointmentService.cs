﻿using ClinicSync.Application.Common;
using ClinicSync.Application.DTO;
using ClinicSync.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicSync.Application.Interfaces.services
{
    public interface IAppointmentService
    {
        Result<List<AppointmentResponse>> GetAllByWeek(DateTime firstDayOfWeek, DateTime lastDayOfWeek);
        Result<Appointment> Create(AppointmentRequest request);
        Result<Appointment> GetById(int id);
        Result<int> Delete(int id);
        Result<Appointment> Update(int id, AppointmentRequest request);
    }
}
