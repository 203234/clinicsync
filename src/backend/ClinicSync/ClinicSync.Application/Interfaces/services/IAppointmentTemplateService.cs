﻿using ClinicSync.Application.Common;
using ClinicSync.Application.DTO;
using ClinicSync.Domain.Entities;

namespace ClinicSync.Application.Interfaces.services
{
    public interface IAppointmentTemplateService
    {
        Result<AppointmentTemplate> GetById(int id);
        Result<Page<AppointmentTemplate>> GetAllPageable(string? searchTerm, int pageNumber, int pageSize);

        Result<List<AppointmentTemplate>> GetAll(string? searchTerm);
        Result<AppointmentTemplate> Add(AppointmentTemplateRequest request);
        Result<AppointmentTemplate> Update(int id, AppointmentTemplateRequest request);
        Result<AppointmentTemplate> Delete(int id);
        Result<int> DeleteRange(string ids);
        Result<int> DeleteAll();
    }
}
