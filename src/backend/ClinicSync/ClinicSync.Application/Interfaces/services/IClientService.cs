﻿using ClinicSync.Application.Common;
using ClinicSync.Application.DTO;
using ClinicSync.Domain.Entities;

namespace ClinicSync.Application.Interfaces.services
{
    public interface IClientService : ICrudService<Client, ClientRequest>
    {

    }
}
