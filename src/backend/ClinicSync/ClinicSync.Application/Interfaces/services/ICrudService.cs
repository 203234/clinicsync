﻿using ClinicSync.Application.Common;
using ClinicSync.Domain.Entities;

namespace ClinicSync.Application.Interfaces.services
{
    public interface ICrudService<T, RequestDTO> where T : BaseEntity
    {
        Result<T> GetById(int id);
        Result<Page<T>> GetAllPageable(string? searchTerm, int pageNumber, int pageSize);

        Result<List<T>> GetAll(string? searchTerm);
        Result<T> Add(RequestDTO request);
        Result<T> Update(int id, RequestDTO request);
        Result<T> Delete(int id);
        Result<int> DeleteRange(string ids);
        Result<int> DeleteAll();
    }
}
