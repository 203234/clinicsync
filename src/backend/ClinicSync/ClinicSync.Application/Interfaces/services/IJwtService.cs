﻿using ClinicSync.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicSync.Application.Interfaces.services
{
    public interface IJwtService
    {
        string GenerateToken(User user);
    }
}
