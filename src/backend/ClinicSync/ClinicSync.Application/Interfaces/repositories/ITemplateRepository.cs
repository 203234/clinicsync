﻿using ClinicSync.Application.DTO;
using ClinicSync.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicSync.Application.Interfaces.repositories
{
    public interface ITemplateRepository : IBaseRepository<AppointmentTemplate>
    {

    }
}
