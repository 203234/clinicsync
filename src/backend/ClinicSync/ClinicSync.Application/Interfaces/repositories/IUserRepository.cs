﻿using ClinicSync.Domain.Entities;

namespace ClinicSync.Application.Interfaces.repositories
{
    public interface IUserRepository
    {
        Task<User> Add(User user);
        Task<User?> GetByUsername(string username);
        Task<User?> GetById(int id);
    }
}
