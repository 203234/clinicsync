﻿using ClinicSync.Application.Common;
using ClinicSync.Domain.Entities;

namespace ClinicSync.Application.Interfaces.repositories
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        public Task<T> GetById(int id);
        Task<Page<T>> GetAllPageable(string? searchTerm, int pageNumber, int pageSize);
        List<T> getAll(string? searchTerm);
        public Task<T> Add(T entity);
        public Task<T> Update(T entity);
        public Task<int> Delete(T entity);
        public Task<int> Delete(int[] ids);
        public Task<int> DeleteAll();
    }
}
