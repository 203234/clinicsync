﻿using ClinicSync.Domain.Entities;

namespace ClinicSync.Application.Interfaces.repositories
{
    public interface IClientNoteRepository : IBaseRepository<ClientNote>
    {
    }
}
