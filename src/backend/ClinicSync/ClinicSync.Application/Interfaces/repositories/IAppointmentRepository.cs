﻿using ClinicSync.Application.DTO;
using ClinicSync.Domain.Entities;
using ClinicSync.Domain.Enums;

namespace ClinicSync.Application.Interfaces.repositories
{
    public interface IAppointmentRepository
    {
        List<Appointment> GetAllByWeek(DateTime start, DateTime end);
        Task<Appointment> GetById(int id);
        Task<Appointment> Create(Appointment entity);
        Task<Appointment> Update(Appointment entity, int? templateId, int? clientId, AppointmentTemplateRequest templateRequest, ClientRequest clientRequest);
        Task Delete(Appointment entity);
    }
}
