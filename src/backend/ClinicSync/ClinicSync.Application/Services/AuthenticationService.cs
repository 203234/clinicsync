﻿using ClinicSync.Application.Common;
using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Application.Interfaces.services;
using ClinicSync.Domain.Entities;

namespace ClinicSync.Application.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUserRepository userRepository;
        private readonly IPasswordHasher passwordHasher;  
        private readonly IJwtService jwtService;

        public AuthenticationService(IUserRepository userRepository,
                                     IPasswordHasher passwordHasher,
                                     IJwtService jwtService)
        {
            this.userRepository = userRepository;
            this.passwordHasher = passwordHasher;
            this.jwtService = jwtService;
        }

        public async Task<Result<string>> Register(CreateUserRequest request)
        {
            var user = await userRepository.GetByUsername(request.Username);
            if (user != null)
            {
                return Result<string>.Failure("Username already exists");
            }
            User AddedUser = new User()
            {
                Username = request.Username,
                Password = passwordHasher.HashPassword(request.Password),
            };

            User result = await userRepository.Add(AddedUser);
            
            if (result != null)
            {
                return Result<string>.Success(jwtService.GenerateToken(result));
            }
            return Result<string>.Failure("Account could not be created. Please try again later.");
        }

        public async Task<Result<string>> Login(LoginRequest loginRequest)
        {
            var user = await userRepository.GetByUsername(loginRequest.Username);
            if (user == null || !passwordHasher.VerifyPassword(loginRequest.Password, user.Password))
            {
                return Result<string>.Failure("Invalid username or password");
            }
            return Result<string>.Success(jwtService.GenerateToken(user));
        }
    }
}
