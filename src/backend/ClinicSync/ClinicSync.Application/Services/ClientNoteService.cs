﻿using ClinicSync.Application.Common;
using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Application.Interfaces.services;
using ClinicSync.Domain;
using ClinicSync.Domain.Entities;

namespace ClinicSync.Application.Services
{
    public class ClientNoteService : IClientNotesService
    {

        readonly private IClientNoteRepository _clientNoteRepository;

        public ClientNoteService(IClientNoteRepository clientNoteRepository) { 
            _clientNoteRepository = clientNoteRepository;
        }

        public Result<Page<ClientNote>> GetAllPageable(string? searchTerm, int pageNumber, int pageSize)
        {
            if (pageNumber < AppConstants.FirstPage)
            {
                return Result<Page<ClientNote>>.Failure("Page number should not be lesser than 1");
            }

            return Result<Page<ClientNote>>.Success(_clientNoteRepository.GetAllPageable(searchTerm, pageNumber, pageSize).Result);
        }

        public Result<ClientNote> GetById(int id)
        {
            var client = _clientNoteRepository.GetById(id);
            if (client == null)
            {
                return Result<ClientNote>.Failure("Client note not found");
            }
            return Result<ClientNote>.Success(client.Result);
        }

        public Result<ClientNote> Add(ClientNoteRequest request)
        {
            ClientNote note = new()
            {
                ClientId = request.CliendId,
                Description = request.Description,
            };

            _clientNoteRepository.Add(note);
            return Result<ClientNote>.Success(note);
        }

        public Result<ClientNote> Delete(int id)
        {
            var note = _clientNoteRepository.GetById(id);
            if (note == null)
            {
                return Result<ClientNote>.Failure("Client note cannot be deleted");
            }

            _clientNoteRepository.Delete(note.Result);
            return Result<ClientNote>.Success(note.Result);
        }

        public Result<int> DeleteAll()
        {
            var result = _clientNoteRepository.DeleteAll();
            if (result.IsFaulted || result.IsCanceled)
            {
                return Result<int>.Failure("Something went wrong...");
            }
            return Result<int>.Success(result.Result);
        }

        public Result<int> DeleteRange(string ids)
        {
            int[] idsArray = ids.Split(",").Select(int.Parse).ToArray();

            var result = _clientNoteRepository.Delete(idsArray);
            if (result.IsFaulted || result.IsCanceled)
            {
                return Result<int>.Failure("Something went wrong...");
            }
            return Result<int>.Success(result.Result);
        }

        public Result<ClientNote> Update(int id, ClientNoteRequest request)
        {
            var note = _clientNoteRepository.GetById(id);

            if (note == null)
            {
                return Result<ClientNote>.Failure("Something went wrong");
            }

            ClientNote updatedNote = note.Result;

            updatedNote.ClientId = request.CliendId;
            updatedNote.Description = request.Description;

            updatedNote.ModifiedAt = DateTime.UtcNow;

            _clientNoteRepository.Update(updatedNote);

            return Result<ClientNote>.Success(updatedNote);
        }

        public Result<List<ClientNote>> GetAll(string? searchTerm)
        {
            throw new NotImplementedException();
        }
    }
}
