﻿using ClinicSync.Application.Common;
using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Application.Interfaces.services;
using ClinicSync.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicSync.Application.Services
{
    public class AppointmentService : IAppointmentService
    {
        private readonly IAppointmentRepository appointmentRepository;
        private readonly IAppointmentTemplateService appointmentTemplateService;
        private readonly IClientService clientService;

        public AppointmentService(IAppointmentRepository appointmentRepository, IClientService clientService, IAppointmentTemplateService appointmentTemplateService)
        {
            this.appointmentRepository = appointmentRepository;
            this.clientService = clientService;
            this.appointmentTemplateService = appointmentTemplateService;
        }

        public Result<Appointment> Create(AppointmentRequest request)
        {
            Client client = new Client();
            AppointmentTemplate appointmentTemplate = new AppointmentTemplate();

            if (request.ClientId == null)
            {
                if (request.ClientRequest != null)
                    client = clientService.Add(request.ClientRequest).Data;
            }
            else
                if(request.ClientId != null)
                    client = clientService.GetById((int)request.ClientId).Data;

            if (string.IsNullOrEmpty(client.FirstName))
                return Result<Appointment>.Failure("Client not found");

            if (request.AppointmentTemplateId == null)
            {
                if (request.TemplateRequest != null)
                {
                    request.TemplateRequest.DurationInMinutes = request.DurationInMinutes;
                    appointmentTemplate = appointmentTemplateService.Add(request.TemplateRequest).Data;
                }
            }
            else
                if (request.AppointmentTemplateId != null)
                    appointmentTemplate = appointmentTemplateService.GetById((int)request.AppointmentTemplateId).Data;

            if (string.IsNullOrEmpty(client.FirstName))
                return Result<Appointment>.Failure("Client not found");

            if (string.IsNullOrEmpty(appointmentTemplate.Name))
                return Result<Appointment>.Failure("Template not found");

            Appointment appointment = new Appointment()
            {
                AppointmentTemplateId = appointmentTemplate.Id,
                ClientId = client.Id,
                Date = request.Date,
                DoneAt = DateTime.Now.AddMinutes(appointmentTemplate.Duration!.Value.Minutes)
            };


            appointmentRepository.Create(appointment);
            return Result<Appointment>.Success(appointment);
        }

        public Result<Appointment> Update(int id, AppointmentRequest request)
        {
            request.TemplateRequest.DurationInMinutes = request.DurationInMinutes;
            Appointment appointment = appointmentRepository.GetById(id).Result;
            if (appointment != null)
            {
                appointment.Date = request.Date;
                this.appointmentRepository.Update(appointment, request.AppointmentTemplateId, request.ClientId, request.TemplateRequest, request.ClientRequest);
                return Result<Appointment>.Success(appointment);
            }

            //if (request.AppointmentTemplateId != null && request.ClientId != null)
            //{
            //    request.TemplateRequest.DurationInMinutes = request.DurationInMinutes;
            //    //appointmentTemplateService.Update((int)request.AppointmentTemplateId, request.TemplateRequest);
            //    //clientService.Update((int)request.ClientId, request.ClientRequest);
                
            //    Appointment appointment = appointmentRepository.GetById(id).Result;
            //    if (appointment != null)
            //    {
            //        appointment.Date = request.Date;
            //        this.appointmentRepository.Update(appointment);
            //        return Result<Appointment>.Success(appointment);
            //    }
            //}
            else if (request.AppointmentTemplateId == null)
            {
                return Result<Appointment>.Failure("Appointment template was not provided");
            }
            else if (request.ClientId == null)
            {
                return Result<Appointment>.Failure("Client was not provided");
            }
            return Result<Appointment>.Failure("Something went wrong...");
        }

        public Result<int> Delete(int id)
        {
            var appointment  = appointmentRepository.GetById(id).Result;
            if (appointment == null)
            {
                return Result<int>.Failure("Appointment not found");
            }
            appointmentRepository.Delete(appointment);
            return Result<int>.Success(1);
        }

        public Result<List<AppointmentResponse>> GetAllByWeek(DateTime firstDayOfWeek, DateTime lastDayOfWeek)
        {

            var appoitments = appointmentRepository.GetAllByWeek(firstDayOfWeek, lastDayOfWeek);
            var appointmentResponses = new List<AppointmentResponse>();

            

            foreach(var appointment in appoitments)
            {
                int col = (int)appointment.Date.DayOfWeek == 0 ? 6 : (int)appointment.Date.DayOfWeek - 1;

                int colspan = ((int)appointment.AppointmentTemplate.Duration.Value.TotalMinutes) / 30;

                int row = (int)appointment.Date.TimeOfDay.TotalMinutes / 30 - 16;

                AppointmentResponse response = new AppointmentResponse()
                {
                    Appointment = appointment,
                    Col = col,
                    Row = row,
                    ColSpan = colspan
                };

                appointmentResponses.Add(response);
                
            }    
            return Result<List<AppointmentResponse>>.Success(appointmentResponses);
        }

        public Result<Appointment> GetById(int id)
        {
            var appointment = appointmentRepository.GetById(id).Result;
            return Result<Appointment>.Success(appointment);
        }
    }
}
