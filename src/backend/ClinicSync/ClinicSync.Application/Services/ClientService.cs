﻿using ClinicSync.Application.Common;
using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Application.Interfaces.services;
using ClinicSync.Domain;
using ClinicSync.Domain.Entities;

namespace ClinicSync.Application.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;


        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public Result<Page<Client>> GetAllPageable(string? searchTerm, int pageNumber, int pageSize)
        {
            if (pageNumber < AppConstants.FirstPage)
            {
                return Result<Page<Client>>.Failure("Page number should not be lesser than 1");
            }

            return Result<Page<Client>>.Success(_clientRepository.GetAllPageable(searchTerm, pageNumber, pageSize).Result);

        }

        public Result<Client> GetById(int id)
        {
            var client = _clientRepository.GetById(id);
            if (client == null)
            {
                return Result<Client>.Failure("Client not found");
            }
            return Result<Client>.Success(client.Result);
        }

        public Result<Client> Add(ClientRequest request)
        {

            Client client = new Client()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Address = request.Adress,
                Phone = request.Phone,
                DateOfBirth = request.DateOfBirth != null ?  DateOnly.Parse(request.DateOfBirth) : null,
                Gender = request.Gender
            };

            var result = _clientRepository.Add(client);
            return Result<Client>.Success(result.Result);
        }

        public Result<Client> Delete(int id)
        {
            var client = _clientRepository.GetById(id);
            if (client == null)
            {
                return Result<Client>.Failure("Client cannot be deleted");
            }

            _clientRepository.Delete(client.Result);
            return Result<Client>.Success(client.Result);
        }

        public Result<int> DeleteAll()
        {
            var result = _clientRepository.DeleteAll();
            if (result.IsFaulted || result.IsCanceled)
            {
                return Result<int>.Failure("Something went wrong...");
            }
            return Result<int>.Success(result.Result);
        }

        public Result<int> DeleteRange(string ids)
        {
            int[] idsArray = ids.Split(",").Select(int.Parse).ToArray();

            var result = _clientRepository.Delete(idsArray);
            if (result.IsFaulted || result.IsCanceled)
            {
                return Result<int>.Failure("Something went wrong...");
            }
            return Result<int>.Success(result.Result);
        }

        public Result<Client> Update(int id, ClientRequest request)
        {
            var client = _clientRepository.GetById(id);

            if (client == null)
            {
                return Result<Client>.Failure("Something went wrong");
            }

            Client updatedCLient = client.Result;

            updatedCLient.FirstName = request.FirstName;
            updatedCLient.LastName = request.LastName;
            updatedCLient.Email = request.Email;
            updatedCLient.Phone = request.Phone;
            updatedCLient.Address = request.Adress;
            updatedCLient.DateOfBirth = request.DateOfBirth != null ? DateOnly.Parse(request.DateOfBirth) : null;
            updatedCLient.Gender = request.Gender;

            updatedCLient.ModifiedAt = DateTime.UtcNow;

            _clientRepository.Update(updatedCLient);

            return Result<Client>.Success(updatedCLient);
        }

        public Result<List<Client>> GetAll(string? searchTerm)
        {
            var clients = _clientRepository.getAll(searchTerm);
            return Result<List<Client>>.Success(clients);
        }
    }
}
