﻿using ClinicSync.Application.Common;
using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Application.Interfaces.services;
using ClinicSync.Domain;
using ClinicSync.Domain.Entities;

namespace ClinicSync.Application.Services
{
    public class AppointmentTemplateService : IAppointmentTemplateService
    {
        private ITemplateRepository _templateRepository;

        public AppointmentTemplateService(ITemplateRepository templateRepository)
        {
            _templateRepository = templateRepository;
        }

        public Result<Page<AppointmentTemplate>> GetAllPageable(string? searchTerm, int pageNumber, int pageSize)
        {
            if (pageNumber < AppConstants.FirstPage)
            {
                return Result<Page<AppointmentTemplate>>.Failure("Page number should not be lesser than 1");
            }

            return Result<Page<AppointmentTemplate>>.Success(_templateRepository.GetAllPageable(searchTerm, pageNumber, pageSize).Result);
        }

        public Result<AppointmentTemplate> GetById(int id)
        {
            var template = _templateRepository.GetById(id);
            if (template == null)
            {
                return Result<AppointmentTemplate>.Failure("Appointment template not found");
            }
            return Result<AppointmentTemplate>.Success(template.Result);
        }

        public Result<AppointmentTemplate> Add(AppointmentTemplateRequest request)
        {
            AppointmentTemplate template = new AppointmentTemplate()
            {
                Name = request.Name,
                Description = request.Description,
                Duration = request.DurationInMinutes.HasValue ? TimeSpan.FromMinutes(request.DurationInMinutes.Value) : null,
                Price = request.Price
            };

            var result = _templateRepository.Add(template);
            return Result<AppointmentTemplate>.Success(result.Result);
        }

        public Result<AppointmentTemplate> Delete(int id)
        {
            var template = _templateRepository.GetById(id);
            if (template == null)
            {
                return Result<AppointmentTemplate>.Failure("Appointment template cannot be deleted");
            }

            _templateRepository.Delete(template.Result);
            return Result<AppointmentTemplate>.Success(template.Result);
        }

        public Result<int> DeleteRange(string ids)
        {
            int[] idsArray = ids.Split(",").Select(int.Parse).ToArray();

            var result = _templateRepository.Delete(idsArray);
            if (result.IsFaulted || result.IsCanceled)
            {
                return Result<int>.Failure("Something went wrong...");
            }
            return Result<int>.Success(result.Result);
        }

        public Result<int> DeleteAll()
        {
           var result = _templateRepository.DeleteAll();
            if (result.IsFaulted || result.IsCanceled)
            {
                return Result<int>.Failure("Something went wrong...");
            }
            return Result<int>.Success(result.Result);
        }

        public Result<AppointmentTemplate> Update(int id, AppointmentTemplateRequest request)
        {
            var template = _templateRepository.GetById(id);

            if (template == null)
            {
                return Result<AppointmentTemplate>.Failure("Something went wrong");
            }

            AppointmentTemplate updatedTemplate = template.Result;

            updatedTemplate.Name = request.Name;
            updatedTemplate.Description = request.Description;
            updatedTemplate.Duration = request.DurationInMinutes.HasValue ? TimeSpan.FromMinutes(request.DurationInMinutes.Value) : null;
            updatedTemplate.Price = request.Price;

            updatedTemplate.ModifiedAt = DateTime.UtcNow;

            _templateRepository.Update(updatedTemplate);

            return Result<AppointmentTemplate>.Success(updatedTemplate);
        }

        public Result<List<AppointmentTemplate>> GetAll(string? searchTerm)
        {
            var templates = _templateRepository.getAll(searchTerm);
            return Result<List<AppointmentTemplate>>.Success(templates);
        }
    }
}
