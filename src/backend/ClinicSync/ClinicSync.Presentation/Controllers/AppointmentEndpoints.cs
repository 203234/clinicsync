﻿using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace ClinicSync.Presentation.Controllers
{
    public static class AppointmentEndpoints
    {

        public static void MapAppointmentEndPoints(this IEndpointRouteBuilder app)
        {
            app.MapPost("/api/appointments", Add);
            app.MapPost("/api/appointments/calendar", GetCalendarData);
            app.MapPost("/api/appointments/{id}", GetById);
            app.MapDelete("/api/appointments/{id}", Delete);
            app.MapPut("/api/appointments/{id}", Update);
        }

        public static async Task<IResult> Add([FromBody] AppointmentRequest request,
                                      [FromServices] IAppointmentService appointmentService)
        {
            var result = appointmentService.Create(request);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }
        public static async Task<IResult> GetCalendarData([FromBody] AppointmentCalednarRequest request,
                              [FromServices] IAppointmentService appointmentService)
        {
            var result = appointmentService.GetAllByWeek(request.firstDayOfWeek, request.lastDayOfWeek);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> GetById([FromRoute] int id,
                              [FromServices] IAppointmentService appointmentService)
        {
            var result = appointmentService.GetById(id);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Delete([FromRoute] int id, [FromServices] IAppointmentService appointmentService)
        {
            var result = appointmentService.Delete(id);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Update([FromBody] AppointmentRequest request,
                                                 [FromRoute] int id,
                                                 [FromServices] IAppointmentService appointmentService)
        {
            var result = appointmentService.Update(id, request);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

    }
}
