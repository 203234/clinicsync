﻿using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.services;
using ClinicSync.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System.Runtime.CompilerServices;

namespace ClinicSync.Presentation.Controllers
{
    [Authorize]
    public static class AppointmentTemplateEndpoints
    {
        public static void MapAppointmentTemplateEndPoints(this IEndpointRouteBuilder app)
        {
            app.MapGet("/api/templates", GetAllPageable);//.RequireAuthorization();
            app.MapPost("/api/templates", Add);
            app.MapPut("/api/templates/{id:int}", Update);
            app.MapDelete("/api/templates/{id:int}", Delete);
            app.MapDelete("/api/templates/", DeleteRange);
            app.MapGet("/api/templates/all", GetAll);
        }

        public static async Task<IResult> GetAllPageable([FromServices] IAppointmentTemplateService appointmentTemplateService,
                                                         [FromQuery] string? searchTerm,
                                                         [FromQuery] int page = AppConstants.FirstPage,
                                                         [FromQuery] int size = AppConstants.PageSize)
        {
            var result = appointmentTemplateService.GetAllPageable(searchTerm, page, size);

            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }

            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Add([FromBody] AppointmentTemplateRequest request,
                                              [FromServices] IAppointmentTemplateService appointmentTemplateService)
        {
            var result = appointmentTemplateService.Add(request);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Update([FromBody] AppointmentTemplateRequest request,
                                                 [FromRoute] int id,
                                                 [FromServices] IAppointmentTemplateService appointmentTemplateService)
        {
            var result = appointmentTemplateService.Update(id, request);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Delete([FromRoute] int id, [FromServices] IAppointmentTemplateService appointmentTemplateService)
        {
            var result = appointmentTemplateService.Delete(id);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> DeleteRange([FromQuery] string? ids,
                                                      [FromQuery] bool? all,
                                                      [FromServices] IAppointmentTemplateService appointmentTemplateService)
        {
            if (all == true)
            {
                var result = appointmentTemplateService.DeleteAll();
                if (result.IsFailure)
                {
                    return Results.BadRequest(result.ErrorMessage);
                }
                return Results.Ok(result.Data);
            }
            else if (ids != null && ids.Length > 0)
            {
                var result = appointmentTemplateService.DeleteRange(ids);
                if (result.IsFailure)
                {
                    return Results.BadRequest(result.ErrorMessage);
                }
                return Results.Ok(result.Data);
            }
            return Results.BadRequest("Cannot process the request");
        }

        public static async Task<IResult> GetAll([FromQuery] string? searchTerm, [FromServices] IAppointmentTemplateService templateService)
        {
            var templates = templateService.GetAll(searchTerm).Data;
            return Results.Ok(templates);
        }
    }
}
