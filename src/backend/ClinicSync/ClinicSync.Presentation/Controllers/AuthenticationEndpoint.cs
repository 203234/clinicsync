﻿using ClinicSync.Domain.Entities;
using ClinicSync.Infrastructure.Services;
using ClinicSync.Presentation.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.services;

namespace ClinicSync.Presentation.Controllers
{
    public static class AuthenticationEndpoint
    {
        public static void MapAuthenticationEndpoints(this IEndpointRouteBuilder app)
        {
            app.MapPost("/api/register", Register);
            app.MapPost("/api/login", Login);
            app.MapGet("/api/test", IsAuthenticated);
        }

        public static async Task<IResult> Register([FromBody] CreateUserRequest request, IAuthenticationService authneticationService)
        {
            var result = await authneticationService.Register(request);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Login([FromBody] LoginRequest request, IAuthenticationService authenticationService)
        {
            var result = await authenticationService.Login(request);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        [Authorize]
        public static Task<IResult> IsAuthenticated()
        {
            return Task.FromResult(Results.Ok("You are authenticated!"));
        }
    }
}
