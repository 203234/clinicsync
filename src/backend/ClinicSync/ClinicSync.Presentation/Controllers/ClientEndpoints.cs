﻿using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.services;
using ClinicSync.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace ClinicSync.Presentation.Controllers
{
    public static class ClientEndpoints
    {
        public static void MapClientEndpoints(this IEndpointRouteBuilder app)
        {
            app.MapGet("/api/clients", GetAllPageable);//.RequireAuthorization();
            app.MapPost("/api/clients", Add);
            app.MapPut("/api/clients/{id:int}", Update);
            app.MapDelete("/api/clients/{id:int}", Delete);
            app.MapDelete("/api/clients/", DeleteRange);
            app.MapGet("/api/clients/all", GetAll);
        }

        public static async Task<IResult> GetAllPageable([FromServices] IClientService clientService,
                                                 [FromQuery] string? searchTerm,
                                                 [FromQuery] int page = AppConstants.FirstPage,
                                                 [FromQuery] int size = AppConstants.PageSize)
        {
            var result = clientService.GetAllPageable(searchTerm, page, size);

            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }

            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Add([FromBody] ClientRequest request,
                                              [FromServices] IClientService clientService)
        {
            var result = clientService.Add(request);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Update([FromBody] ClientRequest request,
                                                 [FromRoute] int id,
                                                 [FromServices] IClientService clientService)
        {
            var result = clientService.Update(id, request);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Delete([FromRoute] int id, [FromServices] IClientService clientService)
        {
            var result = clientService.Delete(id);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> DeleteRange([FromQuery] string? ids,
                                                      [FromQuery] bool? all,
                                                      [FromServices] IClientService clientService)
        {
            if (all == true)
            {
                var result = clientService.DeleteAll();
                if (result.IsFailure)
                {
                    return Results.BadRequest(result.ErrorMessage);
                }
                return Results.Ok(result.Data);
            }
            else if (ids != null && ids.Length > 0)
            {
                var result = clientService.DeleteRange(ids);
                if (result.IsFailure)
                {
                    return Results.BadRequest(result.ErrorMessage);
                }
                return Results.Ok(result.Data);
            }
            return Results.BadRequest("Cannot process the request");
        }


        public static async Task<IResult> GetAll([FromQuery] string? searchTerm, [FromServices] IClientService clientService)
        {
            var clients = clientService.GetAll(searchTerm).Data;
            return Results.Ok(clients);
        }

    }
}
