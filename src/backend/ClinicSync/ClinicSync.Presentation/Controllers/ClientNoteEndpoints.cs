﻿using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.services;
using ClinicSync.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;


namespace ClinicSync.Presentation.Controllers
{
    public static class ClientNoteEndpoints
    {
        public static void MapClientNoteEndpoints(this IEndpointRouteBuilder app)
        {
            app.MapGet("/api/client-notes", GetAllPageable);//.RequireAuthorization();
            app.MapPost("/api/client-notes", Add);
            app.MapPut("/api/client-notes/{id:int}", Update);
            app.MapDelete("/api/client-notes/{id:int}", Delete);
            app.MapDelete("/api/client-notes/", DeleteRange);
        }

        public static async Task<IResult> GetAllPageable([FromServices] IClientNotesService clientNoteService,
                                         [FromQuery] string? searchTerm,
                                         [FromQuery] int page = AppConstants.FirstPage,
                                         [FromQuery] int size = AppConstants.PageSize)
        {
            var result = clientNoteService.GetAllPageable(searchTerm, page, size);

            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }

            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Add([FromBody] ClientNoteRequest request,
                                              [FromServices] IClientNotesService clientNoteService)
        {
            var result = clientNoteService.Add(request);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Update([FromBody] ClientNoteRequest request,
                                                 [FromRoute] int id,
                                                 [FromServices] IClientNotesService clientNoteService)
        {
            var result = clientNoteService.Update(id, request);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> Delete([FromRoute] int id, [FromServices] IClientNotesService clientNoteService)
        {
            var result = clientNoteService.Delete(id);
            if (result.IsFailure)
            {
                return Results.BadRequest(result.ErrorMessage);
            }
            return Results.Ok(result.Data);
        }

        public static async Task<IResult> DeleteRange([FromQuery] string? ids,
                                                      [FromQuery] bool? all,
                                                      [FromServices] IClientNotesService clientNoteService)
        {
            if (all == true)
            {
                var result = clientNoteService.DeleteAll();
                if (result.IsFailure)
                {
                    return Results.BadRequest(result.ErrorMessage);
                }
                return Results.Ok(result.Data);
            }
            else if (ids != null && ids.Length > 0)
            {
                var result = clientNoteService.DeleteRange(ids);
                if (result.IsFailure)
                {
                    return Results.BadRequest(result.ErrorMessage);
                }
                return Results.Ok(result.Data);
            }
            return Results.BadRequest("Cannot process the request");
        }
    }
}
