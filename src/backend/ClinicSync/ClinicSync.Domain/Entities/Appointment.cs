﻿using ClinicSync.Domain.Enums;

namespace ClinicSync.Domain.Entities
{
    public class Appointment : BaseEntity
    {
        public DateTime Date { get; set; }

        public int ClientId { get; set; }
        public Client Client { get; set; }

        public AppointmentStatus AppointmentStatus { get; set; }
        public Status Status { get; set; }

        public AppointmentTemplate? AppointmentTemplate { get; set; }
        public int? AppointmentTemplateId { get; set; }

        public DateTimeOffset? DoneAt { get; set; }

        public Appointment()
        {
            AppointmentStatus = AppointmentStatus.SCHEDULED;
            Status = Status.ENABLED;
        }
    }
}
