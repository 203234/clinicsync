﻿using ClinicSync.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace ClinicSync.Domain.Entities
{
    public class Client : BaseEntity
    {
        public string FirstName { get; set; } = String.Empty;
        public string LastName { get; set; } = String.Empty;
        public string? Address {  get; set; } = String.Empty;
        public string? Phone {  get; set; } = String.Empty;
        [EmailAddress]
        public string? Email {  get; set; } = String.Empty;
        public DateOnly? DateOfBirth { get; set; }
        public Gender? Gender { get; set; }
        public Status Status { get; set; }

        public Client()
        {
            Status = Status.ENABLED;
        }
    }
}
