﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicSync.Domain.Entities
{
    public class ClientNote : BaseEntity
    {
        public int ClientId { get; set; }
        public Client Client { get; set; }
        public string Description { get; set; } = String.Empty;
    }
}
