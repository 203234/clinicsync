﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicSync.Domain.Entities
{
    public class AppointmentTemplate : BaseEntity
    {
        public string Name { get; set; } = String.Empty; // unique key
        public double? Price { get; set; }
        public string? Description { get; set; }
        public TimeSpan? Duration { get; set; }
    }
}
