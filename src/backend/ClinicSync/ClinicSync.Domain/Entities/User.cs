﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ClinicSync.Domain.Entities
{
    public class User : BaseEntity
    {
        [EmailAddress]
        public string Username {  get; set; }

        [JsonIgnore]
        public string Password { get; set; }
    }
}
