﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicSync.Domain
{
    public static class AppConstants
    {
        public const int PageSize = 15;
        public const int FirstPage = 1;
    }
}
