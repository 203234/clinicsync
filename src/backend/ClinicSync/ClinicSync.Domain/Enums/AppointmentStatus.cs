﻿namespace ClinicSync.Domain.Enums
{
    public enum AppointmentStatus
    {
        SCHEDULED,
        IN_PROGRESS,
        COMPLETED,
        CANCELLED
    }
}
