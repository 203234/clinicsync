﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Application.Interfaces.services;
using ClinicSync.Infrastructure.Configurations;
using ClinicSync.Infrastructure.Repositories;
using ClinicSync.Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;

namespace ClinicSync.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddScoped<IJwtService, JwtService>();
            services.AddScoped<IPasswordHasher, PasswordHasher>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ITemplateRepository, TemplateRepository>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<IClientNoteRepository, ClientNoteRepository>();
            services.AddScoped<IAppointmentRepository, AppointmentRepository>();

            return services;
        }
    }
}
