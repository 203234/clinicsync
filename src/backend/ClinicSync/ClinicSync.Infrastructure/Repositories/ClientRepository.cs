﻿using ClinicSync.Application.Common;
using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Domain.Entities;
using ClinicSync.Infrastructure.Persistance;
using Microsoft.EntityFrameworkCore;

namespace ClinicSync.Infrastructure.Repositories
{
    public class ClientRepository : IClientRepository
    {

        private readonly ApplicationDbContext _context;

        public ClientRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Page<Client>> GetAllPageable(string? searchTerm, int pageNumber, int pageSize)
        {
            var query = _context.Clients.AsQueryable();

            if (!string.IsNullOrEmpty(searchTerm))
            {
                query = query.Where(c => c.FirstName.Contains(searchTerm) || c.LastName.Contains(searchTerm));
            }

            var totalElements = await query.CountAsync();

            var clients = await query
            .OrderByDescending(t => t.CreatedAt)
            .Skip((pageNumber - 1) * pageSize)
            .Take(pageSize)
            .AsSplitQuery()
            .AsNoTracking()
            .ToListAsync();

            Page<Client> page = new()
            {
                Content = clients,
                PageNumber = pageNumber,
                PageSize = clients.Count,
                TotalElements = totalElements,
            };

            return page;
        }

        public Task<Client> GetById(int id)
        {
            return _context.Clients.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<Client> Add(Client entity)
        {
            var result = await _context.Clients.AddAsync(entity);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<int> Delete(Client entity)
        {
            _context.Clients.Remove(entity);
            await _context.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<int> Delete(int[] ids)
        {
            return await _context.Clients
                .Where(c => ids.Contains(c.Id))
                .ExecuteDeleteAsync();
            //_context.RemoveRange(ids);

            // SaveChangesAsync is not necessary in this case.
            // The ExecuteDeleteAsync method directly executes the delete operation against the database,
            // so it does not require a call to SaveChangesAsync

        }
        public async Task<int> DeleteAll()
        {
            return await _context.Database.ExecuteSqlRawAsync("TRUNCATE TABLE Clients");
        }

        public async Task<Client> Update(Client entity)
        {
            _context.Clients.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public List<Client> getAll(string? searchTerm)
        {
            var query = _context.Clients.AsQueryable();

            if (!string.IsNullOrEmpty(searchTerm))
            {
                query = query.Where(c => c.FirstName.Contains(searchTerm) || c.LastName.Contains(searchTerm));
            }

            return query.ToList();
        }
    }
}
