﻿using ClinicSync.Application.Common;
using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Domain.Entities;
using ClinicSync.Infrastructure.Persistance;
using Microsoft.EntityFrameworkCore;

namespace ClinicSync.Infrastructure.Repositories
{
    public class ClientNoteRepository : IClientNoteRepository
    {
        readonly private ApplicationDbContext _context;

        public ClientNoteRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Page<ClientNote>> GetAllPageable(string? searchTerm, int pageNumber, int pageSize)
        {
            var query = _context.ClientNotes.AsQueryable();

            if (!string.IsNullOrEmpty(searchTerm))
                query = query.Where(t => t.Description.Contains(searchTerm));

            var totalElements = await query.CountAsync();

            var notes = await query
                .OrderByDescending(t => t.CreatedAt)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .AsSplitQuery()
                .AsNoTracking()
                .ToListAsync();

            Page<ClientNote> page = new()
            {
                Content = notes,
                PageNumber = pageNumber,
                PageSize = notes.Count,
                TotalElements = totalElements
            };

            return page;
        }

        public async Task<ClientNote> GetById(int id)
        {
            return await _context.ClientNotes.FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<ClientNote> Add(ClientNote entity)
        {
            await _context.ClientNotes.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<int> Delete(ClientNote entity)
        {
            _context.ClientNotes.Remove(entity);
            await _context.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<int> Delete(int[] ids)
        {
            var rowsAffected = await _context.ClientNotes
                .Where(i => ids.Contains(i.Id))
                .ExecuteDeleteAsync();

            return rowsAffected;
        }

        public async Task<int> DeleteAll()
        {
            return await _context.Database.ExecuteSqlRawAsync("TRUNCATE TABLE ClientNotes");
        }

        public async Task<ClientNote> Update(ClientNote entity)
        {
            _context.ClientNotes.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public List<ClientNote> getAll(string? searchTerm)
        {
            throw new NotImplementedException();
        }
    }
}
