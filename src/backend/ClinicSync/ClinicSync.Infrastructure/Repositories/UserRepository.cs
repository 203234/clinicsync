﻿using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Domain.Entities;
using ClinicSync.Infrastructure.Persistance;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicSync.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {

        protected readonly ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<User> Add(User user)
        {
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<User?> GetById(int id)
        {
            return await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<User?> GetByUsername(string Username)
        {
            return await _context.Users
                .FirstOrDefaultAsync(x => x.Username == Username);
        }
    }
}