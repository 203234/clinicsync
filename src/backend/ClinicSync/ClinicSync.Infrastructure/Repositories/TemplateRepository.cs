﻿using ClinicSync.Application.Common;
using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Domain.Entities;
using ClinicSync.Infrastructure.Persistance;
using Microsoft.EntityFrameworkCore;

namespace ClinicSync.Infrastructure.Repositories
{
    public class TemplateRepository : ITemplateRepository
    {
        private readonly ApplicationDbContext _context;

        public TemplateRepository(ApplicationDbContext context)
        {
            _context = context;
        }


        public async Task<AppointmentTemplate> GetById(int id)
        {
            return await _context.AppointmentTemplates.FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<Page<AppointmentTemplate>> GetAllPageable(string? searchTerm, int pageNumber, int pageSize)
        {
            var query = _context.AppointmentTemplates.AsQueryable();
                
            if (!string.IsNullOrEmpty(searchTerm))
                query = query.Where(t => t.Name.Contains(searchTerm));

            var totalElements = await query.CountAsync();

            var templates =  await query
                .OrderByDescending(t => t.CreatedAt)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .AsSplitQuery()
                .AsNoTracking()
                .ToListAsync();

            Page<AppointmentTemplate> page = new()
            {
                Content = templates,
                PageNumber = pageNumber,
                PageSize = templates.Count,
                TotalElements = totalElements
            };

            return page;
        }

        public async Task<AppointmentTemplate> Add(AppointmentTemplate entity)
        {
            //var appointments = Enumerable.Range(1, 31).Select(i => new AppointmentTemplate

            //{
            //    Name = $"Tempalte {i}",
            //    Price = i * 10.0,
            //    Description = $"Description for template {i}",
            //    Duration = TimeSpan.FromMinutes(i * 15)
            //});

            //await _context.AppointmentTemplates.AddRangeAsync(appointments);

            var s = await _context.AppointmentTemplates.AddAsync(entity);
            await _context.SaveChangesAsync();
            return s.Entity;
        }

        public async Task<int> Delete(AppointmentTemplate entity)
        {
            _context.AppointmentTemplates.Remove(entity);
            await _context.SaveChangesAsync();
            return entity.Id;
        }

        public async Task<int> Delete(int[] ids)
        {
            var rowsAffected = await _context.AppointmentTemplates
                .Where(i => ids.Contains(i.Id))
                .ExecuteDeleteAsync();

            return rowsAffected;


            //var templates = _context.AppointmentTemplates.Where(i => ids.Contains(i.Id)).ToList();
            //_context.AppointmentTemplates.RemoveRange(templates);
            //return await _context.SaveChangesAsync();

            //var idsString = string.Join(",", entities);
            //return await _context.Database.ExecuteSqlRawAsync($"DELETE FROM AppointmentTemplates WHERE id IN ({idsString})");
        }

        public async Task<int> DeleteAll()
        {
            return await _context.Database.ExecuteSqlRawAsync("TRUNCATE TABLE AppointmentTemplates");
        }

        public async Task<AppointmentTemplate> Update(AppointmentTemplate entity)
        {
            _context.AppointmentTemplates.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public List<AppointmentTemplate> getAll(string? searchTerm)
        {
            var query = _context.AppointmentTemplates.AsQueryable();

            if (!string.IsNullOrEmpty(searchTerm))
                query = query.Where(t => t.Name.Contains(searchTerm));

            return query.ToList();
        }
    }
}
