﻿using ClinicSync.Application.DTO;
using ClinicSync.Application.Interfaces.repositories;
using ClinicSync.Domain.Entities;
using ClinicSync.Infrastructure.Persistance;
using Microsoft.EntityFrameworkCore;

namespace ClinicSync.Infrastructure.Repositories
{
    public class AppointmentRepository : IAppointmentRepository
    {
        private readonly ApplicationDbContext _context;

        public AppointmentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Appointment> GetAllByWeek(DateTime startDate, DateTime endDate)
        {
            endDate = endDate.AddDays(1).AddTicks(-1);
            return _context.Appointments
                .Include(s => s.Client)
                .Include(s => s.AppointmentTemplate)
                .Where(a => a.Date >= startDate && a.Date <= endDate)
                .ToList();
        }

        public async Task<Appointment> GetById(int id)
        {
            return await _context.Appointments.FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task<Appointment> Create(Appointment entity)
        {
            await _context.Appointments.AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task Delete(Appointment entity) 
        {
            _context.Appointments.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<Appointment> Update(Appointment entity, int? templateId, int? clientId, AppointmentTemplateRequest templateRequest, ClientRequest clientRequest)
        {
            if (templateId != null)
            {
               var template = await _context.AppointmentTemplates.FirstOrDefaultAsync(t => t.Id == templateId);
                template.Name = templateRequest.Name;
                template.Description = templateRequest.Description;
                template.Price = templateRequest.Price;
                template.Duration = TimeSpan.FromMinutes((int)templateRequest.DurationInMinutes);
       
                await _context.AppointmentTemplates
                    .Where(t => t.Id == templateId)
                    .ExecuteUpdateAsync(s => s
                    .SetProperty(t => t.Name, t => template.Name)
                    .SetProperty(t => t.Description, t => template.Description)
                    .SetProperty(t => t.Price, t => template.Price)
                    .SetProperty(t => t.Duration, t => TimeSpan.FromMinutes((int)templateRequest.DurationInMinutes)));
                //await _context.SaveChangesAsync();
            }
            if (clientId != null)
            {
                var client = await _context.Clients.FirstOrDefaultAsync(c => c.Id == clientId);
                client.FirstName = clientRequest.FirstName;
                client.LastName = clientRequest.LastName;
                client.Email = clientRequest.Email;
                client.Phone = clientRequest.Phone;
                client.Address = clientRequest.Adress;

                await _context.Clients
                    .Where(t => t.Id == clientId)
                    .ExecuteUpdateAsync(s => s
                        .SetProperty(t => t.FirstName, t => client.FirstName)
                        .SetProperty(t => t.LastName, t => client.LastName)
                        .SetProperty(t => t.Phone, t => client.Phone)
                        .SetProperty(t => t.Email, t => client.Email)
                        .SetProperty(t => t.Address, t => client.Address)
                    );

                //_context.Clients.Update(client);
                //await _context.SaveChangesAsync();
            }

            _context.Appointments.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
    }
}
