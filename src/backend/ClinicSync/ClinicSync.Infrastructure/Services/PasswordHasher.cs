﻿using BCrypt.Net;
using ClinicSync.Application.Interfaces.services;
using Microsoft.AspNetCore.Identity;

namespace ClinicSync.Infrastructure.Services
{
    public class PasswordHasher : IPasswordHasher
    {
        public string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password); // You can specify a work factor, default is 10.
        }

        public bool VerifyPassword(string password, string hashedPassword)
        {
            return BCrypt.Net.BCrypt.Verify(password, hashedPassword);   // .EnhancedVerify is starred.
        }
    }
}
