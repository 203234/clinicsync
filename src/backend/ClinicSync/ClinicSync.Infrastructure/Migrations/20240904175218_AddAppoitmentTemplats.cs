﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ClinicSync.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddAppoitmentTemplats : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AppointmentTemplateId",
                table: "Appointments",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_AppointmentTemplateId",
                table: "Appointments",
                column: "AppointmentTemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointments_AppointmentTemplates_AppointmentTemplateId",
                table: "Appointments",
                column: "AppointmentTemplateId",
                principalTable: "AppointmentTemplates",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Appointments_AppointmentTemplates_AppointmentTemplateId",
                table: "Appointments");

            migrationBuilder.DropIndex(
                name: "IX_Appointments_AppointmentTemplateId",
                table: "Appointments");

            migrationBuilder.DropColumn(
                name: "AppointmentTemplateId",
                table: "Appointments");
        }
    }
}
