﻿using ClinicSync.Domain.Entities;
using ClinicSync.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace ClinicSync.Infrastructure.Persistance
{
    public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
    {
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<AppointmentTemplate> AppointmentTemplates { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientNote> ClientNotes { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ConfigureAppointment(modelBuilder);
            ConfigureClient(modelBuilder);
            ConfigureClientNote(modelBuilder);
            ConfigureUser(modelBuilder);
        }

        private void ConfigureAppointment(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Appointment>(entity =>
            {
                entity.HasOne(c => c.Client)
                      .WithMany()
                      .HasForeignKey(c => c.ClientId);

                entity.Property(a => a.AppointmentStatus)
                    .IsRequired(true)
                    .HasConversion(
                        v => v.ToString(),
                        v => (AppointmentStatus)Enum.Parse(typeof(AppointmentStatus), v)
                    );

                entity.Property(a => a.Status)
                     .IsRequired(true)
                     .HasConversion(
                         v => v.ToString(),
                         v => (Status)Enum.Parse(typeof(Status), v)
                     );

                entity.HasOne(s => s.AppointmentTemplate)
                    .WithMany()
                    .HasForeignKey(s => s.AppointmentTemplateId);
                    

                entity.HasIndex(s => s.ModifiedAt)
                    .IsClustered(false)
                    .IsDescending(true);

                entity.HasKey(s => s.Id);
   


                // .isRequired(false) is not needed if I specify nullabel with '?'
                // , if a property in your entity class is nullable (declared with ?), like DateTimeOffset? or int?,
                // it implies that the corresponding column in the database can be nullable as well
            });
        }
        private void ConfigureClient(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(a => a.Status)
                .IsRequired(true)
                .HasConversion(
                    v => v.ToString(),
                    v => (Status)Enum.Parse(typeof(Status), v)
                );

                entity.Property(a => a.Gender)
                .HasConversion(
                    v => v.ToString(),
                    v => (Gender)Enum.Parse(typeof(Gender), v)
                 );
            });
        }

        private void ConfigureClientNote(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientNote>(entity =>
            {
                entity.HasOne(c => c.Client)
                      .WithMany()
                      .HasForeignKey(c => c.ClientId);
            });
        }

        private void ConfigureUser(ModelBuilder modelBuilder)
        {

        }
    }
}
