﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ClinicSync.Infrastructure.Persistance
{
    public static class DatabaseModule
    {
        public static void AddDatabase(this IServiceCollection collection, IConfiguration configuration)
        {
            string connectionString = configuration.GetConnectionString("DefaultConnection")!;
            collection.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));
        }

        public static async Task<IApplicationBuilder> UseDatabase(this IApplicationBuilder builder)
        {
            await builder.UseAutomaticMigrations();

            return builder;
        }
    }
}
